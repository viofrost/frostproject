
#include "objMain.h"

static GLfloat mod_x = 0,mod_y = 0,mod_z = 0;
static GLfloat mouse_x = -1.0f,mouse_y = -1.0f;
static GLfloat _scal = 0.1f;
static GLfloat _rotate_x = 0.0f;
static GLfloat _rotate_y = 0.0f;

//材质
static GLfloat mat_ambient[]={0.8,0.8,0.8,1.0};

//灯光属性
static GLfloat lightPosition[] = {3.0, 0.0, -1.0, 0.0};
static bool enableLight = false;
static bool enableText  = false;
static bool enableMove  = false;
static bool enableRotate  = false;


static void error_callback(int error, const char* description)
{
    fputs(description, stderr);
}

static void Cursor_callback(GLFWwindow* window,double x,double y)
{
    //cout << enableMove << " X=" << x << " Y=" << y << endl;
    if (enableMove && enableRotate) {
        return;
    }
    if ( enableMove) {
        if (mouse_x < 0.0f || mouse_y < 0.0f) {
            mouse_x = x;
            mouse_y = y;
        }
        else {
            mod_x = (x - mouse_x)/200.0f;
            mod_y = -(y - mouse_y)/200.0f;
        }
    }

    if ( enableRotate) {
        if (mouse_x < 0.0f || mouse_y < 0.0f) {
            mouse_x = x;
            mouse_y = y;
        }
        else {
            _rotate_y += (x - mouse_x)/200.0f;
            _rotate_x += -(y - mouse_y)/200.0f;
        }
    }
}

static void Mouse_Key_CB(GLFWwindow* window,int scancode, int action, int mods)
{
    //cout << scancode << " action=" << action << " mods=" << mods << endl;
    if ( action == GLFW_PRESS) {
        switch (scancode) {
        case GLFW_MOUSE_BUTTON_1:
            enableMove = true;
            break;
        case GLFW_MOUSE_BUTTON_2:
            enableRotate = true;
            break;
        default:
            break;
        }
    }
    else {
        switch (scancode) {
        case GLFW_MOUSE_BUTTON_1:
            enableMove = false;
            mouse_x = -1.0f,mouse_y = -1.0f;
            break;
        case GLFW_MOUSE_BUTTON_2:
            enableRotate = false;
            mouse_x = -1.0f,mouse_y = -1.0f;
            break;
        default:
            break;
        }
    }
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if ( action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        case GLFW_KEY_UP:
            _rotate_x += 0.1;
            break;
        case GLFW_KEY_DOWN:
            _rotate_x -= 0.1;
            break;
        case GLFW_KEY_LEFT:
            _rotate_y += 0.1;
            break;
        case GLFW_KEY_RIGHT:
            _rotate_y -= 0.1;
            break;
        case GLFW_KEY_W:
            mod_y += 0.1;
            break;
        case GLFW_KEY_S:
            mod_y -= 0.1;
            break;
        case GLFW_KEY_A:
            mod_x -= 0.1;
            break;
        case GLFW_KEY_D:
            mod_x += 0.1;
            break;
        case GLFW_KEY_E:
            if ( enableText) {
                glEnable( GL_TEXTURE_2D);
            }else
            {
                glDisable( GL_TEXTURE_2D);
            }
            enableText = !enableText;
            break;
        case GLFW_KEY_Q:
            if ( enableLight) {
                glEnable( GL_LIGHTING);
            }else
            {
                glDisable( GL_LIGHTING);
            }
            enableLight = !enableLight;
            break;
        default:
            break;
        }
    }
}

static void open_light()
{
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);//指定深度比较中使用的数值
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    //glEnable(GL_COLOR_MATERIAL); // 使用颜色材质
}

static void set_light(int num)
{
    switch ( num) {
    case 11:

        break;
    default:
        GLfloat lightModeColor[] = {1.0, 1.0, 1.0, 1.0};
        glLightModeli( GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE);
        glLightModelfv( GL_LIGHT_MODEL_AMBIENT, lightModeColor);
        break;
    }

}

static void set_material(MtlDate * mtl)
{
    if (mtl == nullptr) {
        return;
    }
    GLfloat matAmbient[] = {0.6, 0.6, 0.6, 1.0};        //环境颜色
    GLfloat matDiffuse[]   = {0.35, 0.35, 0.35, 1.0};   //散射颜色
    GLfloat matAmbDif[]   = {0.5, 0.5, 0.5, 1.0};       //环境颜色和散射颜色
    GLfloat matSpecular[] = {0.2, 0.2, 0.2, 1.0};       //镜面反射颜色
    GLfloat shine[] = {5.0};                            //镜面反射指数
    GLfloat matEmission[] = {0.3, 0.1, 0.1, 1.0};       //发射光颜色

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mtl->Ka);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mtl->Kd);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mtl->Ka);  // 将背景颜色和散射颜色设置成同一颜色
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mtl->Ks);
    shine[0] = mtl->Ns;
    glMaterialfv(GL_FRONT, GL_SHININESS, shine);
    glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);  // 用来模拟物体发光的效果，但这不是光源
}


int objMain(int argc, char** argv)
{
    FilePaser fp;
    string filename;
    ObjModel* model =nullptr;
    do {
        cout << "input file name:";
        cin >> filename;
        model = fp.OpenObjFile( filename + ".obj");
    } while (model == nullptr);

    GLFWwindow* window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    window = glfwCreateWindow(640, 480, "Obj Model", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, Cursor_callback);
    glfwSetMouseButtonCallback(window, Mouse_Key_CB);

    //打开灯光
    open_light();
    //设置材质
    set_material( model->getMatl());

    //打开并读取纹理
    glEnable(GL_TEXTURE_2D);
    unsigned int Texture = Uilts::LoadTex( "Grass.bmp");
    while (!glfwWindowShouldClose(window))
    {
        float ratio;
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        ratio = width / (float) height;
        glViewport(0, 0, width, height);
        glClearColor (0.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT);

        glClearDepth( 1);
        glClear(GL_DEPTH_BUFFER_BIT);


        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-ratio, ratio, -1.f, 1.f, 0.0f, 1000.0f);
        glTranslatef( mod_x, mod_y, mod_z);
        glScalef( _scal, _scal, _scal);

        //重置灯光
        set_light( model->getLightMode());

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glRotatef(_rotate_x * 20.f, 1.f, 0.f, 0.f);
        glRotatef(_rotate_y * 20.f, 0.f, 1.f, 0.f);

        //绑定纹理
        if (Texture != 0) {
            glBindTexture(GL_TEXTURE_2D, Texture);
        }
        //批量渲染
        for (int mod_inx = 0; mod_inx < model->getModelNum(); ++mod_inx) {
            vector<float> versData = model->getVertexList( mod_inx);
            int count = 0;
            if ( !versData.empty()) {
                //glColor3f( 0.0f, 0.8f, 0.f);
                switch ( model->mode( mod_inx)) {
                case K_V:
                    count = versData.size()/ 3; // V3F=3一个点
                    glInterleavedArrays( GL_V3F, 0, &versData[0]);
                    break;
                case K_VN:
                    count = versData.size()/ 6; // N3V3=6一个点
                    glInterleavedArrays( GL_N3F_V3F, 0, &versData[0]);
                    break;
                case K_VT:
                    count = versData.size()/ 5; // T2V3=5一个点
                    glInterleavedArrays( GL_T2F_V3F, 0, &versData[0]);
                    break;
                case K_VTN:
                    count = versData.size()/ 8; //T2N3V3=8一个点
                    glInterleavedArrays( GL_T2F_N3F_V3F, 0, &versData[0]);
                    break;
                default:
                    break;
                }
                glDrawArrays( GL_TRIANGLES, 0, count);
            }
        }

        //显示并处理事件
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    //删除模型数据
    delete model;
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
