#ifndef UILTS_H
#define UILTS_H

#include <vector>
#include <string>
#include "GLFW/glfw3.h"

#define DWORD unsigned long

using namespace std;

class Uilts
{
public:
    /**
     * @brief split 按Pattern分割字符串
     * @param str 要分割的字符串
     * @param pattern 分割模式
     * @return 分割后的字符串列表
     */
    static vector<string> split(const string& str, string pattern);

    /**
     * @brief trim 除去字符串前后空白
     * @param text 要去掉空白的字符串
     * @return 去掉空白后的字符串
     */
    static string& trim(string& text);

    static unsigned int GenTex(string Image);

    /**
     * @brief LoadTex 载入一个纹理
     * @param Image 纹理名字
     * @return 纹理句柄
     */
    static unsigned int LoadTex(string Image);

    /**
     * @brief LoadBMP 读取一张BMP图片
     * @param Image[in]   图片名
     * @param bWidth[out]  图片宽度
     * @param bHeight[out] 图片高度
     * @return        BMP的RGB数组
     *
     * @warning 你需要在适当的时候free()返回的数据
     */
    static unsigned char* LoadBMP(string Image, unsigned long &bWidth, unsigned long &bHeight);

private:
    Uilts();

};

#endif // UILTS_H
