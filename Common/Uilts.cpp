#include "Uilts.h"



vector<string> Uilts::split(const string& str, string pattern)
{
    std::string::size_type pos;
    std::vector<std::string> result;
    std::string bufstr = str + pattern;//扩展字符串以方便操作
    int size=bufstr.size();

    for(int i=0; i<size; i++)
    {
        pos=bufstr.find(pattern,i);
        if(pos<size)
        {
            std::string s=bufstr.substr(i,pos-i);
            result.push_back(s);
            i=pos+pattern.size()-1;
        }
    }
    return result;
}

string& Uilts::trim(string &text)
{
    {
        if(!text.empty())
        {
            text.erase(0, text.find_first_not_of( " \n\r\t" ));
            text.erase(text.find_last_not_of( " \n\r\t") + 1);
        }
        return text;
    }
}

unsigned int Uilts::GenTex(string Image)
{
    unsigned int Texture;
    glGenTextures(1, &Texture);

    unsigned long bWidth = 0;
    unsigned long bHeight = 0;

    unsigned char *data = LoadBMP(Image, bWidth, bHeight);

    glBindTexture(GL_TEXTURE_2D, Texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bWidth, bHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, data);
    glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
    glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);

    // glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    //
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // glTexGenf(GL_R, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
    // glTexGenf(GL_Q, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);

    return Texture;
}

unsigned int Uilts::LoadTex(string Image)
{
    unsigned int Texture;
    glGenTextures(1, &Texture);

    unsigned long bWidth = 0;
    unsigned long bHeight = 0;

    unsigned char *data = LoadBMP(Image, bWidth, bHeight);

    glBindTexture(GL_TEXTURE_2D, Texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bWidth, bHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, data);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

    if (data)
        free(data);

    return Texture;
}

unsigned char* Uilts::LoadBMP(string Image, unsigned long &bWidth, unsigned long &bHeight )
{
    FILE* img = NULL;
    img = fopen(Image.c_str(),"rb");

    if ( img == NULL) {
        return 0;
    }

    DWORD size = 0;

    fseek(img,18,SEEK_SET);
    fread(&bWidth,4,1,img);
    fread(&bHeight,4,1,img);
    fseek(img,0,SEEK_END);
    size = ftell(img) - 54;

    unsigned char *data = (unsigned char*)malloc(size);

    fseek(img,54,SEEK_SET);    // image data
    fread(data,size,1,img);

    fclose(img);

    return data;
}
