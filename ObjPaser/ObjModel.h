#ifndef OBJMODEL_H
#define OBJMODEL_H

#include <string>
#include <vector>
#include <map>

using namespace std;

struct MtlDate
{
    string name;
    float Ns;       //反射指数
    float Ni;       //折射值
    float d;        //渐隐指数
    float Ka[4];    //环境反射
    float Kd[4];    //漫反射
    float Ks[4];    //镜反射
    int illum;      //使用的材质(1~10)
};

//该模型数据模式
enum DataMode
{
    K_UNKNOW,
    K_V,
    K_VT,
    K_VN,
    K_VTN
};

class ObjModel
{
public:
    ObjModel();
    ~ObjModel();
    vector<float> getVertexList(int index) const;
    void setVertexData(float v);

    MtlDate* getMatl(string key) const;
    MtlDate* getMatl() const;
    int getLightMode() const;
    void setMtlMap(MtlDate* date);

    DataMode mode(int index) const;
    DataMode mode() const;
    void setMode(const DataMode &mode);
    void addNewModel(); //添加一个模型数据
    int getModelNum();
private:
    vector<vector<float>> m_vertexData;     //模型数据
    vector<MtlDate* > m_mtlMap;     //材质数据
    vector<DataMode > m_modeMap;    //子模型模式
};

#endif // OBJMODEL_H
