#ifndef FILEPASER_H
#define FILEPASER_H

#include <fstream>
#include <iostream>
#include <assert.h>
#include "ObjModel.h"

#include "../Common/Uilts.h"

using namespace std;


class FilePaser
{
public:
    FilePaser();
    ObjModel* OpenObjFile(string fileName);


protected:
    void AnalyzeVertex( vector<string> list);
    void AnalyzeNormal( vector<string> list);
    void AnalyzeTexVer( vector<string> list);
    void AnalyzeMtlLib( ObjModel* model, string fileName);

    void AnalyzeFaces(ObjModel* model, vector<string> list);
    void ParseVertexIdex(ObjModel* model, string data);

    void resetDateList();

private:
    vector<float> m_v_list;  //3个数据一组
    vector<float> m_vt_list; //2个数据一组
    vector<float> m_vn_list; //3个数据一组
};

#endif // FILEPASER_H
