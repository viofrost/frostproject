#include "ObjModel.h"

ObjModel::ObjModel()
    :m_vertexData(),
      m_mtlMap(),
      m_modeMap()
{
}

ObjModel::~ObjModel()
{
    for (auto itor : m_mtlMap) {
        delete itor;
    }
    m_mtlMap.clear();
}

vector<float> ObjModel::getVertexList(int index) const
{
    return m_vertexData[index];
}

void ObjModel::setVertexData(float v)
{
    (m_vertexData[m_vertexData.size() -1 ]).push_back(v);
}

MtlDate* ObjModel::getMatl(string key) const
{
    for (auto itor : m_mtlMap) {
        if (itor->name == key) {
            return itor;
        }
    }
    return nullptr;
}

MtlDate *ObjModel::getMatl() const
{
    if (m_mtlMap.size() > 0) {
        return m_mtlMap[0];
    }
    return nullptr;
}

int ObjModel::getLightMode() const
{
    if ( m_mtlMap.size() > 0) {
        return m_mtlMap[0]->illum;
    }
    return 0;
}

void ObjModel::setMtlMap(MtlDate* date)
{
    m_mtlMap.push_back( date);
}
DataMode ObjModel::mode(int index) const
{
    return m_modeMap[index];
}

DataMode ObjModel::mode() const
{
    return m_modeMap.back();
}

void ObjModel::setMode(const DataMode &mode)
{
    m_modeMap[m_modeMap.size()-1] =  mode;
}

void ObjModel::addNewModel()
{
    m_modeMap.push_back( K_UNKNOW);
    vector<float> data;
    m_vertexData.push_back( data);
}

int ObjModel::getModelNum()
{
    return m_vertexData.size();
}



