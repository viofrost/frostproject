#include "FilePaser.h"

FilePaser::FilePaser()
{

}

ObjModel* FilePaser::OpenObjFile(string fileName)
{
    ifstream file;
    file.open(fileName, ios::in);
    if( !file)
    {
        cout << "Open File error!" << endl;
        return nullptr;
    }
    string lineword;
    size_t lineNum = 0;

    ObjModel* model = new ObjModel();

    while ( getline( file, lineword))
    {
        ++lineNum;
        //分割字符串
        Uilts::trim( lineword);
        vector<string> tokens = Uilts::split( lineword, " ");
        //跳过不符合规范的行
        if (tokens.size() < 2) {
            continue;
        }
        string key = tokens[0];
        tokens.erase( tokens.begin()); //移除关键字
        if( key == "v")
        {
            AnalyzeVertex(tokens);
        }
        else if( key == "vn")
        {
            AnalyzeNormal(tokens);
        }
        else if( key == "vt")
        {
            AnalyzeTexVer(tokens);
        }
        else if( key == "f")
        {
            if ( model->getModelNum() < 1) {
                model->addNewModel();
            }
            if ( model->mode() == K_UNKNOW) {
                vector<string> indexList = Uilts::split( tokens[0], "/");
                //设置对象模式
                switch ( indexList.size()) {
                case 3:
                    if ( indexList[1] == "") {
                        model->setMode( K_VN);
                    }
                    else {
                        model->setMode( K_VTN);
                    }

                    break;
                case 2:
                    model->setMode( K_VT);
                    break;
                case 1:
                    model->setMode( K_V);
                    break;
                default:
                    break;
                }
            }
            AnalyzeFaces( model, tokens);
        }
        else if( key == "g")
        {
            model->addNewModel();
        }
        else if( key.substr(0,1) == "#")
        {
            continue;
        }
        else if( key == "usemtl")
        {
            //            if( tokens.size() > 0)
            //            {
            //                model->getMatl( tokens[0]);
            //            }

        }
        else if( key == "mtllib")
        {
            if( tokens.size() > 0)
            {
                AnalyzeMtlLib( model, tokens[0]);
            }
        }
        else
        {
            cout << "the line:" << lineNum << " unknow key word: " << lineword << endl;
        }
    }
    //关闭文件
    file.close();
    return model;
}

void FilePaser::AnalyzeVertex( vector<string> list)
{
    for (auto itor : list)
    {
        float f = atof( itor.c_str());
        m_v_list.push_back(f);
    }

}

void FilePaser::AnalyzeNormal(vector<string> list)
{
    for (auto itor : list)
    {
        float f = atof( itor.c_str());
        m_vn_list.push_back(f);
    }
}

void FilePaser::AnalyzeTexVer( vector<string> list)
{
    if (list.size() >= 2) {
        m_vt_list.push_back( atof( list[0].c_str()));
        m_vt_list.push_back( atof( list[1].c_str()));
    }
    //    for (auto itor : list)
    //    {
    //        float f = atof( itor.c_str());
    //        m_vt_list.push_back(f);
    //    }
}

void FilePaser::AnalyzeMtlLib( ObjModel* model, string fileName)
{
    ifstream file;
    file.open( fileName, ios::in);
    if( !file)
    {
        cout << "Open MtlLib File error!" << endl;
        return ;
    }

    string lineword;
    MtlDate* mtl = nullptr;
    while ( getline( file, lineword))
    {
        //分割字符串
        Uilts::trim( lineword);
        vector<string> tokens = Uilts::split( lineword, " ");

        //跳过不符合规范的行
        if (tokens.size() < 2) {
            continue;
        }


        string key = tokens[0];
        tokens.erase( tokens.begin()); //移除关键字
        if( key.substr(0,1) == "#")
        {
            continue;
        }
        else if( key == "newmtl")
        {
            if ( mtl != nullptr) {
                model->setMtlMap( mtl);
            }
            assert( tokens.size() == 1, "newmtl MUST has a name ");
            mtl = new MtlDate();
            mtl->name = tokens[0];
        }
        else if( key == "Ks")
        {
            assert( tokens.size() == 3, "Ks Must has 3 parm ");
            for (int index = 0; index < tokens.size(); ++index) {
                mtl->Ks[index] = atof( tokens[ index].c_str());
            }
            mtl->Ks[3] = 1.0f;
        }
        else if( key == "Ka")
        {
            assert( tokens.size() == 3, "Ka Must has 3 parm ");
            for (int index = 0; index < tokens.size(); ++index) {
                mtl->Ka[index] = atof( tokens[ index].c_str());
            }
            mtl->Ka[3] = 1.0f;
        }
        else if( key == "Kd")
        {
            assert( tokens.size() == 3, "Kd Must has 3 parm ");
            for (int index = 0; index < tokens.size(); ++index) {
                mtl->Kd[index] = atof( tokens[ index].c_str());
            }
            mtl->Kd[3] = 1.0f;
        }
        else if( key == "Ns")
        {
            assert( tokens.size() == 1, "Ns Must has a parm ");
            mtl->Ns = atof( tokens[ 0].c_str());
        }
        else if( key == "Ni")
        {
            assert( tokens.size() == 1, "Ni Must has a parm ");
            mtl->Ni = atof( tokens[ 0].c_str());
        }
        else if( key == "d")
        {
            assert( tokens.size() == 1, "d Must has a parm ");
            mtl->d = atof( tokens[ 0].c_str());
        }
        else if( key == "illum")
        {
            assert( tokens.size() == 1, "illum Must has a parm ");
            mtl->illum = atoi( tokens[ 0].c_str());
        }
        else
        {
            if ( tokens.size() > 0) {
                cout << "unknow key:" << key << " " << tokens[0] << endl;
            }
        }
    }
    if ( mtl != nullptr) {
        model->setMtlMap( mtl);
    }

    //关闭文件
    file.close();
}

void FilePaser::AnalyzeFaces(ObjModel* model, vector<string> list)
{
    //可能是四边形顶点，需要转化为2个三角形
    switch ( list.size()) {
    case 3:
        for (auto itor : list) {
            ParseVertexIdex(model, itor);
        }
        break;
    case 4:
        for (auto itor : list) {
            ParseVertexIdex(model, itor);
        }

        //第二个三角形读取边数为 4 1 3
        ParseVertexIdex(model, list[0]);
        ParseVertexIdex(model, list[2]);
        break;
    case 5:
        for (auto itor : list) {
            ParseVertexIdex(model, itor);
        }

        //第二个三角形读取边数为 4 5 1
        ParseVertexIdex(model, list[0]);

        //第三个三角形读取边数为 3 4 1
        ParseVertexIdex(model, list[2]);
        ParseVertexIdex(model, list[3]);
        ParseVertexIdex(model, list[0]);
        break;
    default:
        cout << "wrong data. the faces size is:" << list.size() << ".progma can't parse"  << endl;
        break;
    }
}

void FilePaser::ParseVertexIdex(ObjModel* model, string data)
{
    int v_idx = 0, vt_idx = 0, vn_idx = 0;
    vector<string> indexList = Uilts::split( data, "/");
    //当前面编码方式 v | v/t | v//n | v/t/n .根据解释出来数据读取相应索引
    switch (indexList.size()) {
    case 3:
        vn_idx = atoi(indexList[2].c_str());
    case 2:
        vt_idx = atoi(indexList[1].c_str()); // 当为V//N模式，则为无效索引(0)。读取对应数据时会被跳过
    case 1:
        v_idx = atoi(indexList[0].c_str());
        break;
    default:
        cout << " this index out of rang" << endl;
        return;
    }

    //存入模型按照 GL_T2F_C3F_V3F格式

    //存入纹理坐标--T2F
    if (m_vt_list.size() != 0) {
        if( vt_idx > 0) //模型文件中包含法线
        {
            vt_idx = ( ( vt_idx-1) % m_vt_list.size() )  * 2; //修正对应索引
            model->setVertexData( m_vt_list[vt_idx]);
            model->setVertexData( m_vt_list[vt_idx + 1]);
        }
        else
        {
            cout << " texCoords is not index data" << endl;
        }
    }

    //存入法线--C3F
    if( m_vn_list.size() != 0) //模型文件中包含法线
    {
        vn_idx = ( vn_idx-1) * 3; //修正对应索引
        if (vn_idx >= 0 && vn_idx + 2 < m_vn_list.size()) {
            model->setVertexData( m_vn_list[vn_idx]);
            model->setVertexData( m_vn_list[vn_idx + 1]);
            model->setVertexData( m_vn_list[vn_idx + 2]);
        }
        else
        {
            cout << " Normal is not index data" << endl;
        }
    }
    else
    {
        //需要自己计算
    }

    //存入顶点坐标--V3F
    v_idx = ( v_idx-1) * 3; //修正对应索引
    if (v_idx >= 0 && v_idx + 2 < m_v_list.size()) {
        model->setVertexData( m_v_list[v_idx]);
        model->setVertexData( m_v_list[v_idx + 1]);
        model->setVertexData( m_v_list[v_idx + 2]);
    }
    else
    {
        cout << " Vertex is not index data" << endl;
    }
}

void FilePaser::resetDateList()
{
    m_v_list.clear();
    m_vn_list.clear();
    m_vt_list.clear();
}
