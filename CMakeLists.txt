project(FrostGlTest)
cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST)

#定义链接属性
# Debug: /MT
 set(CMAKE_C_FLAGS_DEBUG   "${CMAKE_C_FLAGS_DEBUG}     /Zi /Od /Ob0 /RTC1 /DDEBUG")
 set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}   /Zi /Od /Ob0 /RTC1 /DDEBUG")
# Release: /MT
set(CMAKE_C_FLAGS_RELEASE   "   /MT /O2 /Ob2 /DNDEBUG")
set(CMAKE_CXX_FLAGS_RELEASE " /MT /O2 /Ob2 /DNDEBUG")

# 链接选项
set(CMAKE_EXE_LINKER_FLAGS "/MANIFEST:NO /NODEFAULTLIB:libcmtD.lib /NODEFAULTLIB:libcmt.lib")

set(INCU_LIST
    include/GLFW/glfw3.h
    include/GLFW/glfw3native.h
)

#添加源代码目录
aux_source_directory(./ObjPaser INCU_LIST)
aux_source_directory(./Common INCU_LIST)

add_executable(${PROJECT_NAME} ${SRC_LIST} ${INCU_LIST})

#添加文件夹路径
include_directories(
    include

)

link_directories(${CMAKE_SOURCE_DIR}/libs)
#message( "")

target_link_libraries(${PROJECT_NAME} opengl32)
target_link_libraries(${PROJECT_NAME} glu32)
target_link_libraries(${PROJECT_NAME} ${CMAKE_SOURCE_DIR}/libs/glfw3.lib)
