#include "objMain.h"
#include "heigtMap.h"

using namespace std;

int main(int argc, char** argv)
{
    int num =0;
    do {
        cout << "input: \n    1-open OBJ Model View.\n    2-open Height Map.\n\n\n";
        cin >> num;
    } while ( num > 2 || num < 1);
    switch ( num) {
    case 1:
        cout << "W S A D: move model.\nUP DOWN LEFT RIGHT: rotate model.\nQ:set Light.\n\n";
        return objMain(argc, argv);
    default:
        break;
    }
    return heighMain(argc, argv);

}
